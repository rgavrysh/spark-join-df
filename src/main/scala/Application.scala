import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

import scala.util.{Failure, Success, Try}

object Application {

  def main(args: Array[String]): Unit = {
    println("Starting ...")
    if (args.length < 3) {
      println("Provide 2 input file names, and name to parquet-output file.")
      System.exit(1)
    } else {
      val productPriceFileName = args(0)
      val competitorProductsFileName = args(1)
      val parquetFileName = args(2)

      dataSourceTransformation(productPriceFileName, competitorProductsFileName, parquetFileName)
    }
  }

  def dataSourceTransformation(ppFileName: String, cpFileName: String, dataOutputFileName: String) = {

    val productPriceSchema = new StructType(Array(
      StructField("PROD_LEAF_PCD", StringType, false),
      StructField("LOCATION_LEAF_PCD", StringType, false),
      StructField("CURRENT_PRICE", DoubleType, false),
      StructField("CURRENT_ACV", DoubleType, true),
      StructField("EFFECTIVE_DATE", TimestampType, false)))

    val compProdSchema = new StructType(Array(
      StructField("PROD_LEAF_PCD", StringType, false),
      StructField("LOCATION_LEAF_PCD", StringType, false),
      StructField("COMPETITOR_LEAF_PCD", StringType, false),
      StructField("COMP_PROD_LEAF_PCD", StringType, false),
      StructField("CURRENT_PRICE", DoubleType, false),
      StructField("EFFECTIVE_DATE", TimestampType, false),
      StructField("DATA_QUALITY_INDICATOR", IntegerType, false)))

    val sparkSession = SparkSession.builder().appName("ss-practice").getOrCreate()

    val productPriceDF = readCSVDataSourceWithSchema(sparkSession, productPriceSchema, ppFileName).persist()
    val competitorProductDF = readCSVDataSourceWithSchema(sparkSession, compProdSchema, cpFileName).persist()

    val joinedDF = for {
      ppDF <- Try(validatePPDF(productPriceDF))
      cpDF <- Try(validateCPDF(competitorProductDF))
      resultedDF <- Try(joinDataFrames(ppDF, cpDF))
    } yield resultedDF

    joinedDF match {
      case Success(resultDataFrame) => resultDataFrame.write.parquet("target/" + dataOutputFileName)
      case Failure(exception) => {
        sparkSession.stop()
        throw exception
      }
    }

    sparkSession.stop()
  }

  private def joinDataFrames(validatedProductsDF: DataFrame, validatedCompetitorProductsDF: DataFrame) = {
    validatedProductsDF
      .select("PROD_LEAF_PCD")
      .join(
        validatedCompetitorProductsDF
          .select(
            col("PROD_LEAF_PCD"),
            struct("LOCATION_LEAF_PCD", "COMPETITOR_LEAF_PCD", "COMP_PROD_LEAF_PCD",
              "CURRENT_PRICE", "EFFECTIVE_DATE", "DATA_QUALITY_INDICATOR")
              .as("COMPLEX_CP")
          ),
        "PROD_LEAF_PCD")
      .groupBy(col("PROD_LEAF_PCD"))
      .agg(collect_list("COMPLEX_CP").as("COMPLEX_CP"))
  }

  private def readCSVDataSourceWithSchema(sparkSession: SparkSession, schema: StructType, fileName: String) = {
    sparkSession.read.format("org.apache.spark.csv")
      .schema(schema)
      .option("header", true)
      .csv(fileName)
  }

  private def validatePPDF(dataFrame: DataFrame): DataFrame = {

    val successCriteria = columnMaxTextLength(col("PROD_LEAF_PCD"))
      .and(columnMaxTextLength(col("LOCATION_LEAF_PCD")))
      .and(columnNumericNegative(col("CURRENT_PRICE")))

    validate(dataFrame, successCriteria)
  }

  private def validateCPDF(dataFrame: DataFrame): DataFrame = {

    val successCriteria = columnMaxTextLength(col("PROD_LEAF_PCD"))
      .and(columnMaxTextLength(col("LOCATION_LEAF_PCD")))
      .and(columnMaxTextLength(col("COMPETITOR_LEAF_PCD")))
      .and(columnMaxTextLength(col("COMP_PROD_LEAF_PCD")))
      .and(columnNumericNegative(col("CURRENT_PRICE")))

    validate(dataFrame, successCriteria)
  }

  private def validate(dataFrame: DataFrame, successCriteria: Column): DataFrame = {

    val invalidRows = dataFrame
      .filter(
        not(
          successCriteria
        )
      )
      .collect()

    if (invalidRows.length == 0) {
      dataFrame
    } else {
      throw new IllegalStateException("DataFrame not valid\n" + invalidRows.mkString("\n"))
    }
  }

  private val maxTextLength = 50

  private def columnMaxTextLength(column: Column) = {
    length(column).leq(maxTextLength)
  }

  private def columnNumericNegative(column: Column) = {
    column.geq(0)
  }
}
